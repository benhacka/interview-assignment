import glob
import os
import unittest

from convertor.abstract_base_convertor import AbstractFileConvertor
from main import (assert_source_files, decode_source, encode_decoded_source,
                  ENCODED_DIR, SOURCE_DIR)


def check_source():
    try:
        return assert_source_files() or True
    except AssertionError:
        return False


class TestEncoder(unittest.TestCase):
    def test_encoder(self):
        self.assertTrue(check_source(), 'no source files')
        decode_source()
        encode_decoded_source()
        for file in glob.glob(str(ENCODED_DIR / 'encoded*')):
            with open(file) as encoded_file:
                encoded_result = encoded_file.read()
            file_name = AbstractFileConvertor.output_name_generator(
                file, 'encoded', 'source', SOURCE_DIR)
            self.assertTrue(os.path.isfile(file_name), file_name)
            with open(file_name) as source_file:
                source_result = source_file.read()
            self.assertEqual(encoded_result, source_result)


if __name__ == '__main__':
    unittest.main()
