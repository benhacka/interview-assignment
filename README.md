## Тестовое задание с преобразователями текста 

#### Входные данные
Имеет питоновский пакет *convertor* со следующим списком модулей:  
-   *abstract_base_convertor.py*
-   *decode_convertor.py*
-   *encode_convertor.py*
-   *trash_generator.py*
  
В модуле *abstract_base_convertor* лежит абстрактный класс - интерфейс для производных подклассов:
```python
# abstract_base_convertor.py
import os


class AbstractFileConvertor:
    """Abstract base - interface for 1337 encoder / decoder
    """
    def __init__(self, key_file: [str, ...] = None, **_):
        assert key_file is None or os.path.isfile(key_file)
        self._latter_mapper = self._create_map(key_file)

    def _create_map(self, file: [str, ...], **kwargs) -> dict[str, str]:
        """A method for generating map (dict) for encode/decode text
        :param file: the optional path to a file with text
        :return: the encode/decode dict, return empty dict if file is None
        """
        raise NotImplementedError

    def _convert_text(self, input_text: str) -> str:
        """A method for the convert the text
        (encode -> decode | decode -> encode)
        :param input_text: the text to convert
        :return: converted text
        """
        raise NotImplementedError

    @staticmethod
    def output_name_generator(input_file_name: str, input_prefix: str,
                              output_prefix: str, output_dir: str):
        """A method for getting output file name based on input one and prefix
        :return: output name
        """
        _, name = os.path.split(input_file_name)
        name, ext = os.path.splitext(name)
        common_suffix = name.removeprefix(input_prefix)
        return os.path.join(output_dir, f'{output_prefix}{common_suffix}{ext}')

    def convert_files(self, *, input_dir: str, input_mask: str,
                      output_dir: str, input_prefix: str,
                      output_prefix: str) -> list[str]:
        """A method for converting all files
        :param input_dir: the path to an input dir
        :param input_mask: the input file pattern name
        :param output_dir: the path to an output dir
        :param input_prefix: the input prefix for _output_name_generator
        :param output_prefix: the output prefix for _output_name_generator
        :return: the list of converted strings
        """
        raise NotImplementedError

```
В корне проекта (рядом с пакетом) лежит файл *key*, который содержит словарь для конвертации в текстовом виде.  
В приведенном варианте словаря все символы в первом ряду уникальные, то есть каждый декодированный символ можно трактовать однозначно.  
`cat key`:
```
A: 4, /-\, /_\, @, /\, Д, а
B: 8,|3, 13, |}, |:, |8, 18, 6, |B, |8, lo, |o, j3, ß, в, ь
C: <, {, [, (, ©, ¢, с
D: |), |}, |], |>,
E: 3, £, ₤, €,е
F: |=, ph, |#, |", ƒ
G: [, -, [+, 6, C-,
H: #, 4, |-|, [-], {-}, }-{, }{, |=|, [=], {=}, /-/, (-), )-(, :-:, I+I, н
I: !, |, 1, 9
J: _|, _/, _7, 9,[1] _), _], _}
K: |<, 1<, l<, |{, l{
L: 1, |_, |, ][
M: |\/|, ^^, /\/\, /X\, []\/][, []V[], ][\\//][, (V),//., .\\, N\, м
N: |\|, /\/, /V, ][\\][, И, и, п
O: 0, (), [], {}, <>, Ø, oh, Θ, о, ө
P: |o, |O, |>, |*, |°, |D, /o, []D, |7, р
Q: O_, 9, (,), 0,kw,
R: |2, 12, .-, |^, l2, Я, ®
S: 5, $, §,
T: 7, +, 7`, '|' , `|` , ~|~ , -|-, '][', т
U: |_|, \_\, /_/, \_/, (_), [_], {_}
V: \/
W: \^/, \/\/, (/\), |/\|, \X/, \\', '//, VV, \_|_/, \\//\\//, Ш, 2u, \V/,
X: ><, %, *, }{, )(, Ж,
Y: ¥, `/, \|/, Ч, ү, у
Z: 2, 5, 7_, >_,(/),
```
Также в корне лежи баш скрипт для генерации источников исходного текста *generator.sh*
```shell script
mkdir -p source
python3 -c 'import this' | \
python3 -c "import sys;import re;import string;input_dir='source';[open(f'{input_dir}/source{index}', 'w').write(line) for index, line in enumerate(re.sub(f'[{string.punctuation}]', '', sys.stdin.read().lower()).split('\n')) if line.strip()]"
```
И все в том же корне проекта лежит файл *main.py*, в котором реализована логика вызова методов классов из пакета *convertor*
```python
#!/usr/bin/env python3
import os
from pathlib import Path

from convertor.decode_convertor import Decoder
from convertor.trash_generator import Trasher
# from convertor.encode_convertor import Encoder

ROOT_PATH = Path(__file__).parent
KEY_FILE = ROOT_PATH / 'key'
SOURCE_DIR = ROOT_PATH / 'sources'
DECODED_DIR = ROOT_PATH / 'decoded_sources'
ENCODED_DIR = ROOT_PATH / 'encoded_sources'


def assert_source_files():
    assert SOURCE_DIR.exists() and os.listdir(SOURCE_DIR)


def decode_source():
    decoder = Decoder(KEY_FILE)
    return decoder.convert_files(input_dir=SOURCE_DIR,
                                 input_mask='source*',
                                 output_dir=DECODED_DIR,
                                 input_prefix='source',
                                 output_prefix='decoded')


def generate_trash_files():
    trasher = Trasher()
    return trasher.convert_files(input_dir=DECODED_DIR,
                                 input_mask='decoded*',
                                 output_dir=DECODED_DIR,
                                 input_prefix='decoded',
                                 output_prefix='')


def encode_decoded_source():
    raise NotImplementedError()


def main():
    assert_source_files()
    decode_source()
    generate_trash_files()
    encode_decoded_source()


if __name__ == '__main__':
    main()
```
Перед запуском *main.py* был запущен *generator.sh*, который сгенерировал файлы в корневой подпапке *sources*.  
Конечной целью выполения скрипта является папка **encoded_sources**, в которой будут содержаться файлы encodedN, где N - номер соответствующий равный соответствующем конвертированному файлу в папке **decoded_sources**  
Символы кодируются по первому значению из ряда в словаре key.
##### Пример
`cat decoded_sources/foo5`
```
7#!5 !5 1337 3><4|\/||o13
```

`cat encoded_sources/bar5`
```text
this is leet example
```
`cat sources/foobar5`
```
this is leet example
```
#### Задания:
1. Необходимо написать вызывающую функцию `encode_decoded_source` и реализацию класса `Encoder`, с публичным методом `convert_files`.
2. Предложить по возможности вариант рефакторинга классов для соблюдения *правил ООП*.
3. Предложить по возможности вариант рефакторинга классов для соблюдения *DRY*.
4. (*) Попытаться объяснить, что за дичь происходит в `generator.sh`
5. (*) Написать небольшой unittest, покрывающий метод `convert_files` 
  
  
  
  
# Так тут остановка печати для решающего!
### Это хинты для нас (не давать, а то всю мету с хуйями спалим, рано ещё)
Во-первых, как пожелал Серега, в ридми должен быть **хуй**  
Поэтому: 
# хуй
## хуй
### хуй
#### хуй
#### кок

**1 пункт**  
если будет прям жопа с 1 пунктом, то можно дать исходник `encode_convertor.py` - распечатать отдельно, как и `trash_generator.py`, они понадобятся для 2 и 3 задания. Если человек не сможет написать даже уже с имеющимся шаблоном (где по сути меняется только одна функция), то тут видимо гг уже, дальше нет особо смысла что-то спрашивать.  

Посмотреть насколько реализация отличается от имеющейся, возможно указать на какие-то косяки, если они будут, но незначительные (на значительные тоже наверное нужно указать, но тыкать иначе).  

Если человек в стрессовой ситуации написания кода на листочке додумается отсортировать словарь по длине ключей, чтобы мелкие составные символы не экранировали другие, то это вин (например С(<) может затереть X(><) или W(\/\/) может замениться на 2 V(\/).  

Если `_convert_text` через регулярки, то это замедлит все в несколько раз (не хорошо).  

Если вдруг человек начнет писать докстринги, то нужно остановить и предложить охладить траханье.  

Можно спросить про shebang - первая строчка с решеткой, ну типа зачем она нужна, хотя если человек на винде, то скорее по приколу. 


**2 пункт (тут уже нужно дать исходники всех файлов)**  
Тут фишка в том, что обсер в наследовании, а подсказка прям в имени файла, генератор это не хера не конвертор, для конверторов нужен отдельный класс, унаследованный от абстрактного и уже от него наследовать конверторы.

**3 пункт - он от части базируется на 2**
`convert_files` и `_convert_text` выносится в самый базовый класс, по хорошему второй надо бы переименовать
Функция для чтения словаря из файла (в исходниках `_file_to_dict`) выносится в созданную прослойку, которая появилась во 2 пункте
И в конечно итоге получается, что во всех (условно) конечных классах переопределяется только метод `_create_map`, все остальное у них абсолютно общее, в идеале его можно наверху переименовать, чтобы трэшер вызывал, что-то более нейтральное, а пониже у общего класса для конверторов сделать оберточку.

**4 пункт**
Просто инлайн скриптик, который через пайп получает строку дзен питона (import this), потом преобразует все в нижний регистр, регулярками вычещает пунктуацию и спецсимволы, все это в лист по строчкам и сохраняет в отдельные файлики.

**5 пункт**
Ну это видимо какой-то сверхразум, если хватило времени и готов писать юниттесты, впрочем может он думает, что если он вдруг остановится, то с ним продолжат *introduce yourself*, тогда я понимаю его от части, это самая завальная часть интервью. Примеры тестов есть в папке test, они конечно хуевенькие малек, но в целом пойдут, свою задачу выполняют.


